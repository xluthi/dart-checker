Dart Checker
------------

An app for Android, that assists players of darts in counting their scores.

Game type:  X01, SET/LEG and FREE mode for training, CRICKET

Player:     1-8

The app includes game statistics, undo function, double and single out, checkout suggestions, several cricket variants…

[<img src="https://f-droid.org/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/com.DartChecker/)
